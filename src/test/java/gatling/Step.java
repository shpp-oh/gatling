package gatling;

import io.gatling.javaapi.core.ChainBuilder;
import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.http.HttpDsl;

import static io.gatling.javaapi.core.CoreDsl.StringBody;

public class Step {
    public static ChainBuilder postRequest = CoreDsl.exec(
            HttpDsl.http("Post")
                    .post("/api/tasks")
                    .header("Content-Type", "application/json")
                    .body(StringBody("{\n" +
                            "  \"id\": 0,\n" +
                            "  \"name\": \"string\",\n" +
                            "  \"dateTime\": \"2023-01-12T09:09:23.970Z\",\n" +
                            "  \"status\": \"PLANNED\"\n" +
                            "}"))
                    .check(HttpDsl.status().is(200))
    );

    public static ChainBuilder putRequest = CoreDsl.exec(
            HttpDsl.http("Put")
                    .put("/api/tasks/1")
                    .header("Content-Type", "application/json")
                    .body(StringBody("{\n" +
                            "  \"id\": 0,\n" +
                            "  \"name\": \"string updated\",\n" +
                            "  \"dateTime\": \"2023-01-12T09:09:23.970Z\",\n" +
                            "  \"status\": \"WORK_IN_PROGRESS\"\n" +
                            "}"))
                    .check(HttpDsl.status().is(200))
    );

    public static ChainBuilder getOneRequest = CoreDsl.exec(
            HttpDsl.http("Get one task")
                    .get("/api/tasks/1")
                    .check(HttpDsl.status().is(200))
    );

    public static ChainBuilder getAllRequest = CoreDsl.exec(
            HttpDsl.http("Get all tasks")
                    .get("/api/tasks")
                    .check(HttpDsl.status().is(200))
    );

    public static ChainBuilder deleteOneRequest = CoreDsl.exec(
            HttpDsl.http("Delete one task")
                    .delete("/api/tasks/1")
                    .check(HttpDsl.status().is(200))
    );
}
