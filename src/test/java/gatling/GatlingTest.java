package gatling;

import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpDsl;
import io.gatling.javaapi.http.HttpProtocolBuilder;

public class GatlingTest extends Simulation {
    HttpProtocolBuilder httpProtocol = HttpDsl.http
            .baseUrl("http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com")
//            .baseUrl("http://localhost:5000")
            .basicAuth("admin","admin");

    public GatlingTest() {
        this.setUp(
                Scenario.postScenario.injectOpen(
                        CoreDsl.constantUsersPerSec(1).during(1)),
                Scenario.putScenario.injectOpen(
                        CoreDsl.constantUsersPerSec(1).during(1)),
                Scenario.getOneScenario.injectOpen(
                        CoreDsl.constantUsersPerSec(1).during(1)),
                Scenario.getAllScenario.injectOpen(
                        CoreDsl.constantUsersPerSec(10).during(10)),
                Scenario.deleteOneScenario.injectOpen(
                        CoreDsl.constantUsersPerSec(1).during(1))

        ).protocols(httpProtocol);
    }
}
